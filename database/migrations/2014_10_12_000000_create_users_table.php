<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_name');
            $table->string('name_en');
            $table->string('name_kh');
            $table->string('email')->unique();
            $table->string('gender');
            $table->string('nation');
            $table->string('tel');
            $table->dateTime('dob');
            $table->string('password');
            $table->string('address');
            $table->string('imgages');
            $table->tinyInteger('activate')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
