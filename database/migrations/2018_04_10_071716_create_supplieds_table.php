<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplieds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('suppliers');
            $table->string('email');
            $table->string('tel');
            $table->string('address');
            $table->string('contact_name');
            $table->tinyInteger('activate')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplieds');
    }
}
