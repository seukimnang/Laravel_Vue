
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.VueRouter = require('vue-router').default;

window.VueAxios = require('vue-axios').default;

window.Axios = require('axios').default;

let AppLayout = require('./components/App.vue');

// show the list customer template
const Listcustomer = Vue.component('Listcustomer', require('./components/Listcustomer.vue'));

// add customer template
const Addcustomer = Vue.component('Addcustomer', require('./components/Addcustomer.vue'));

// edit customer template 
const Editcustomer = Vue.component('Editcustomer', require('./components/Editcustomer.vue'));

// delete customer template 
const Deletecustomer = Vue.component('Deletecustomer', require('./components/Deletecustomer.vue'));

// view single customer template
const Viewcustomer = Vue.component('Viewcustomer', require('./components/Viewcustomer.vue'));

// registering Modules
Vue.use(VueRouter , VueAxios , axios);

const routes = [
	{
		name : 'Listcustomer',
		path : '/',
		component : Listcustomer
	},
	{
		name : 'Addcustomer',
		path : '/add-customer',
		component : Addcustomer
	},
	{
		name : 'Editcustomer',
		path : '/edit/:id',
		component : Editcustomer
	},
	{
		name : 'Deletecustomer',
		path : '/delete-customer',
		component : Deletecustomer
	},
	{
		name : 'Viewcustomer',
		path : '/view/:id',
		component : Viewcustomer
	}
];

const router = new VueRouter({ mode: 'history', routes: routes});

new Vue(
	Vue.util.extend(
		{ router },
		AppLayout
	)
).$mount('#app');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// import customer from './components/customer/index.vue';
// import createcustomer from './components/customer/create.vue';

// const app = new Vue({
//     el: '#app',
//     components : { customer }
// });
